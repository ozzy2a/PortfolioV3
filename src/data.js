export const projects = [
    {
        title: "Bleu Méditerranée",
        subtitle: "Projet de fin de Terminale ISN",
        description:
            "Création du site web d'une agence de voyage factice uniquement pour le projet final de Terminal S ISN.",
        techno:"Html, Css, Photoshop",
        image: "./ISN.png",
        link: "https://cheleo07.github.io/CrystalMer.github.io/",
    },
    {
        title: "E-comBox",
        subtitle: "Projet de première année de Bts Sio",
        description:
            "Développement de l'application e-combox, permettant d'installer des outils " +
            "d'e-commerce comme Prestashop pour les élèves de BTS MUC sous forme de dock.",
        techno:"NodeJs, Docker, Wordpress ",
        link:"http://crcm-tl.fr/index.php/diplomes/mercatique/bts-mco/bts-mco-les-ressources-pedagogiques/l-e-combox/l-e-combox-presentation-de-l-application/la-solution-e-combox",
        image: "ecombox.png",
    },
    {
        title: "Disneyland® Paris",
        subtitle: "Stage de première année de Bts Sio",
        description:
            "Développement sur la nouvelle application mobile de Disneyland® Paris.  " +
            "Création d'un outil permettant d'automatiser les tests unitaire et fonctionnels de certaines vues pour aider les testeurs, " +
            "ainsi qu'une vue permettant d'accéder aux paramètres du mobile.",
        image: "./disney.jpg",
        techno:"React Native, Postman (API), Javascript ",
        link: "https://www.disneylandparis.com/fr-fr/application-mobile/",
    },
    {
        title: "PPE",
        subtitle: "Projets de Bts et License Pro",
        description:
            "Divers projets personnels encadrés effectués durant ma dernière année de Bts Sio et celle de license professionnelle Dawin." +
            " De multiples languages et technologies ont été utilisés comme Java, Java EE, Php, Javascript et ses frameworks...",
        techno:"Javascript, Java EE, Php, Sql",
        image: "./student-project.jpg",
    },
    {
        title: "Module Energie",
        subtitle: "Alternance de license pro Dawin",
        description:
            "Développement d'un module intégrable à Ignition, logiciel de création d'application de supervision industrielle, permettant d'inclure de " +
            "nouvelles fonctions scripts pour calculer la consommation d'énergie d'automates, de les récupérer et les manipuler dans le logiciel. ",
        techno:"Java, Python, MySql ",
        image: "./energie.jpg",
    },
    {
        title: "Module Composant",
        subtitle: "Alternance de license pro Dawin",
        description:
            "Développement d'un module intégrable à Ignition, logiciel de création d'application de supervision industrielle, permettant d'inclure de " +
            "nouveaux composants à la bibliothèque de composants du logiciel, provenant de bibliothèques Javascript.",
        techno:"Java, React.Js, Node.Js, TypeScript, Javascript, Scss.",
        image: "./composant.png",
    },
];
export const skills = [
    "Html et Css",
    "Javascript (ReactJs, AngularJs, VueJs et NodeJs)",
    "Java",
    "Php",
    "Python",
    "Base de donnée SQL",
    "Notion de DevOps et d'Agilité",
    "Photoshop",
];
export const experiences = [
    {
        title: "Stage et alternance / Axone-io",
        subtitle: "Mérignac (Bordeaux), Janvier - Mars 2020 et Septembre 2020 - Septembre 2021",
        description:
            "Découverte de l’informatique industrielle et de l’automatisation au sein de l'équipe d'Axone-io, une entreprise proposant des solutions innovantes " +
            "pour l’automatisme et la supervision industrielle." +
            " Renforcement de mes capacités en Java et Sql lors de mon stage et mise en pratique des connaissances acquisent lors de ma formation en alternance.",
        image: "./Axone-io.png",
    },
    {
        title: "Stage / Disneyland® Paris",
        subtitle: "Chessy (Île-de-France), Mai - Aout 2019",
        description:
            "Stage de première année de Bts Sio au sein de l'équipe de développeur de l'application mobile de Disneyland® Paris. " +
            "Découverte du développement mobile, de React Native, de git et des requêtes API avec Postman.",
        image: "./disney-2.png",
    },
];
export const parcours = [
    {
        title: "License Pro Dawin en alternance",
        date: "2021",
        description:"IUT de Gradignan, Bordeaux",
        secondTitle:"Développement en Applications web et Innovation Numérique ",
        secondDesc:"Approfondissement des connaissances en frontend et backend, du développement web mobile ainsi que la prise en main " +
            "de projets full stack.",
        link:"https://www.iut.u-bordeaux.fr/info/assets/ressources/20141030LPROIUTBXINFODAWINw.pdf",
        techno:"NodeJs, Android Studio, Ruby, .Net, NoSql, DevOps, WebGl (ThreeJS)",
    },
    {
        title: "Bts Sio option Slam",
        date: "2020",
        description:"Lycée Gustave Eiffel, Bordeaux",
        secondTitle:"Services Informatiques aux Organisations option Solutions Logicielles et applications Métiers",
        secondDesc:"Apprentissage de la conception, du développement, déploiement et maintenance d'une solution applicative, de la sécurisation de celle-ci " +
            "ainsi que la gestion de bases de donnée.",
        link:"https://www.orientation.com/diplomes/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers",
        techno:"Java, Java EE, Javascript, AngularJs, React, VueJs, C#, Python, Sql",
    },
    {
        title: "Bac S spécialisation ISN",
        date: "2018",
        description:"Lycée Fesch, Ajaccio",
        secondTitle:"Informatique et Science du Numérique",
        secondDesc:"Découverte du monde du numérique, acquisition des notions de base de la programmation, apprentissage de la conduite" +
            " et présentation d'un projet.",
        link:"https://rotek.fr/specialite-isn-informatique-sciences-numerique-bac/",
        techno:"Html, Css, Photoshop",
    },
];
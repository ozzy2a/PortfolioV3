import React from "react";
import {AcademicCapIcon} from "@heroicons/react/solid";
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import { parcours } from "../data";
import {Typography} from "@mui/material";

export default function Parcours() {
    return (
        <section id="parcours">
            <div className="container px-5 py-10 mx-auto text-center">
                <AcademicCapIcon className="w-10 inline-block mb-4" />
                <h1 className="sm:text-4xl text-3xl font-medium title-font text-white mb-12">
                    Mon parcours professionnel
                </h1>
                <React.Fragment>
                    <Timeline position="alternate">
                        {parcours.map((parcour) =>
                            <TimelineItem>
                                <TimelineOppositeContent>
                                    {parcour.date}
                                </TimelineOppositeContent>
                                <TimelineSeparator>
                                    <TimelineDot color="info"/>
                                    <TimelineConnector />
                                </TimelineSeparator>
                                <TimelineContent sx={{ py: '1px', px: 2 }}>
                                    <a href={parcour.link}
                                        className="sm:w-1/2 w-100 p-4">
                                        <div className="flex relative">
                                            <div className="absolute inset-0 w-full h-full object-cover object-center">
                                                <Typography variant="h6" component="span" className="text-white">
                                                    {parcour.title}
                                                </Typography>
                                                <Typography>{parcour.description}</Typography>
                                            </div>
                                            <div className="text-center px-5 py-3 relative z-10 w-full border-2 border-gray-800 bg-gray-900 opacity-0 hover:opacity-100">
                                                <h4 className="tracking-widest text-sm title-font font-medium text-blue-400 mb-1">
                                                    {parcour.secondTitle}
                                                </h4>
                                                <p>
                                                    {parcour.secondDesc}
                                                </p>
                                                <p className="text-white mb-3">
                                                    {parcour.techno}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </TimelineContent>
                            </TimelineItem>
                        )}
                    </Timeline>
                </React.Fragment>
            </div>
        </section>
    );
}